# Análisis II / Análisis Matemático II / Matemática 3

2do cuatrimestre 2023 \
Universidad de Buenos Aires

![Bellezas matemáticas](portada.jpg)

## Prácticas

| Nro | Título                                                                                                |Enunciado                                                                                          | Solución                                                                                                      |
|-----|-------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------|
| 0   | Repaso de integración y cambio de variables | [Enunciado](https://gitlab.com/faustomartinez/uba-analisis-ii/-/blob/main/practicas/enunciados/practica0.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-analisis-ii/-/blob/main/practicas/soluciones/practica5.hs)
| 1   | Curvas, integral de longitud de arco e integrales curvilíneas |[Enunciado](https://gitlab.com/faustomartinez/uba-analisis-ii/-/blob/main/practicas/enunciados/practica1.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-analisis-ii/-/blob/main/practicas/soluciones/practica1.pdf)
| 2   | Teorema de Green |[Enunciado](https://gitlab.com/faustomartinez/uba-analisis-ii/-/blob/main/practicas/enunciados/practica2.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-analisis-ii/-/blob/main/practicas/soluciones/practica2.pdf)
| 3   | Integrales de superficie |[Enunciado](https://gitlab.com/faustomartinez/uba-analisis-ii/-/blob/main/practicas/enunciados/practica3.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-analisis-ii/-/blob/main/practicas/soluciones/practica3.pdf)
| 4   | Teoremas de Stokes y de Gauss. Campos conservativos. Aplicaciones |[Enunciado](https://gitlab.com/faustomartinez/uba-analisis-ii/-/blob/main/practicas/enunciados/practica4.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-analisis-ii/-/blob/main/practicas/soluciones/practica4.pdf)
| 5   | Ecuaciones Diferenciales |[Enunciado](https://gitlab.com/faustomartinez/uba-analisis-ii/-/blob/main/practicas/enunciados/practica5.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-analisis-ii/-/blob/main/practicas/soluciones/practica5.pdf)
| 6   | Ecuaciones de segundo orden y sistemas de primer orden. |[Enunciado](https://gitlab.com/faustomartinez/uba-analisis-ii/-/blob/main/practicas/enunciados/practica6.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-analisis-ii/-/blob/main/practicas/soluciones/practica6.pdf)
| 7   | Diagramas de fase |[Enunciado](https://gitlab.com/faustomartinez/uba-analisis-ii/-/blob/main/practicas/enunciados/practica7.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-analisis-ii/-/blob/main/practicas/soluciones/practica7.pdf)

## Parciales

| Nro | Solución                                                                                          | 
|-----|----------------------------------------------------------------------------------------------------|
| 1   | [Solución](https://gitlab.com/faustomartinez/uba-analisis-ii/-/blob/main/1er-parcial/1er-parcial.pdf) | 
| 2   | [Solución](https://gitlab.com/faustomartinez/uba-analisis-ii/-/blob/main/2do-parcial/2do-parcial.pdf) | 